<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['api', 'guest', 'XSS', 'CORS'], 'prefix' => 'v1'], function () {
	Route::post('auth/login', 'ApiController@doLogin');
	Route::get('auth/user/{username}', 'ApiController@getUserData');
	Route::get('barang/list', 'ApiController@getListBarang');
	Route::get('barang/search/{keys}', 'ApiController@getSearchBarang');
	Route::post('customer/add', 'ApiController@addCustomer');
	Route::get('barcode/scan', 'ApiController@scanBarcode');
	Route::get('categoryPrice/list', 'ApiController@getCategoryPrice');
	Route::get('business/list', 'ApiController@getBusiness');
	Route::get('city/list', 'ApiController@getCities');
	Route::get('customerGroup/list', 'ApiController@getCustomerGroup');
	Route::get('warehouse/list', 'ApiController@getListWarehouse');
	Route::post('cek/pelunasan', 'ApiController@getPelunasanByCabang');
	Route::get('customer/list', 'ApiController@getListCustomer');
	Route::post('cek/piutang', 'ApiController@getPiutangByCustomer');
	Route::get('filter/piutang/{kode_nota}/{date}/{customer_name}', 'ApiController@getFilterPiutang');
	Route::get('filter/pelunasan/{kode_nota}/{date}/{warehouse_name}', 'ApiController@getFilterPelunasan');
	Route::get('barang/image/{id}', 'ApiController@getBarangImage');
	Route::get('delivery/{delivery_id}', 'ApiController@getKonfirmasiDO');
	Route::post('cart/update', 'ApiController@updateDeliveredStock');
	Route::post('cart/confirmed', 'ApiController@setConfirmedDo');
});