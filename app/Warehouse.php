<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    //
    protected $table = 'warehouses';
    protected $fillable = ['warehouse_id', 'warehouse_name', 'warehouse_telp', 'warehouse_address', 'warehouse_fax', 'warehouse_pic', 'warehouse_code'];
    public $timestamps = false;
}
