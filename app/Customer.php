<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //
    protected $table = 'customers';
    protected $fillable = ['customer_id', 'customer_name', 'busines_id', 'customer_group_id', 'customer_address', 'customer_store', 'customer_telp', 'customer_hp', 'customer_npwp', 'customer_npwp_name', 'customer_npwp_address', 'customer_mail',
		'city_id', 'customer_img', 'category_price_id', 'customer_limit_kredit', 'customer_limit_card', 'customer_limit_day',
		'customer_card_no', 'customer_pic_name', 'customer_pic_telp', 'customer_pic_address'];

	public $timestamps = false;
}
