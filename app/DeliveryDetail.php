<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryDetail extends Model
{
    //
    protected $table = 'delivery_details';
    protected $fillable = ['delivery_detail_id', 'delivery_id', 'delivery_detail_code', 'delivery_detail_status', 'warehouse_id', 'delivery_detail_type', 'delivery_detail_date'];
    public $timestamps = false;
}
