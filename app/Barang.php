<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    //
    protected $table = 'items';
    protected $fillable = ['item_id', 'item_clas_id', 'item_sub_clas_id', 'item_name', 'brand_id', 'item_barcode', 'unit_id',
							'item_per_unit', 'item_min', 'item_max', 'item_last_price', 'item_price1', 'item_price2', 'item_price3', 'item_price4', 'item_price5', 'item_cost', 'item_type_id', 'item_weight'];

	public $timestamps = false;
}
