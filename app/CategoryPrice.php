<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryPrice extends Model
{
    //
    protected $table = 'category_prices';
    public $timestamps = false;
}
