<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    //
    protected $table = 'deliveries';
    protected $fillable = ['delivery_id', 'delivery_date', 'employee_id', 'nota_id', 'delivery_cost', 'delifery_freight_cost', 'delivery_lock', 'delivery_request'];

    public $timestamps = false;
}
