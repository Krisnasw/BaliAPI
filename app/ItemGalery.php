<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemGalery extends Model
{
    //
    protected $table = 'item_galeries';
    protected $fillable = ['item_galery_id', 'item_id', 'item_galery_file'];

    public $timestamps = false;
}
