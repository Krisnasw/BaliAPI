<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Validator;
use File;
use JWTAuth;
use App\User;
use App\Barang;
use App\Customer;
use App\Business;
use App\CustomerGroup;
use App\City;
use App\CategoryPrice;
use App\Nota;
use App\Warehouse;
use App\ItemGalery;
use App\Delivery;

class ApiController extends Controller
{
    //
    public function doLogin(Request $request)
    {
      $valid = Validator::make($request->all(), [
        'username' => 'required|string',
        'password' => 'required'
      ]);

      if ($valid->passes()) {
        # code...
        $cek = User::where('user_username', $request->username)->get();
        if (count($cek) <= 0) {
          # code...
          return response(['error' => true, 'message' => 'Akun Tidak Terdaftar'], 200);
        } else {
          try {
            $md5Login = User::where('user_username', $request->username)->where('user_password', md5($request->password))->where('user_active_status', 1)->first();

            if ($md5Login) {
              # code...
              return response(['error' => false, 'message' => 'Login Berhasil', 'data' => $md5Login['user_username']], 200);
            } else {
              return response(['error' => true, 'message' => 'Username atau Password Salah'], 200);
            }
          } catch (JWTException $e) {
            return response(['error' => true, 'message' => 'Tidak Dapat Membuat Token'], 200);
          }
        }
      } else {
        return response(['error' => true, 'message' => 'Username atau Password Masih Kosong'], 200);
      }
    }

    public function getUserData($username)
    {
      $user = User::where('user_username', base64_decode($username))->first();
      if (count($user) <= 0) {
        # code...
        return response(['error' => true, 'message' => 'User Tidak Ditemukan', 'data' => []], 200);
      } else {
        return response(['error' => false, 'message' => 'User Ditemukan', 'data' => $user], 200);
      }
    }

    public function getListBarang()
    {
      $item = Barang::orderBy('item_id', 'DESC')->get();
      if (count($item) <= 0) {
        # code...
        return response(['error' => true, 'message' => 'Barang Tidak Ada', 'data' => []], 200);
      } else {
        return response(['error' => false, 'message' => 'Barang Ditemukan', 'data' => $item], 200);
      }
    }

    public function getSearchBarang($keys)
    {
      $item = Barang::where('item_name', 'LIKE', '%' . $keys . '%')->get();
      if (count($item) <= 0) {
        # code...
        return response(['error' => true, 'message' => 'Barang Tidak Ditemukan', 'data' => []], 200);
      } else {
        return response(['error' => false, 'message' => 'Barang Ditemukan', 'data' => $item], 200);
      }
    }

    public function getCustomerGroup()
    {
      $item = CustomerGroup::orderBy('customer_group_id', 'DESC')->get();
      if (count($item) <= 0) {
        # code...
        return response(['error' => true, 'message' => 'Customer Group Kosong', 'data' => []], 200);
      } else {
        return response(['error' => false, 'message' => 'Customer Group Ditemukan', 'data' => $item], 200);
      }
    }

    public function getBusiness()
    {
      $item = Business::orderBy('busines_id', 'DESC')->get();
      if (count($item) <= 0) {
        # code...
        return response(['error' => true, 'message' => 'Business Group Kosong', 'data' => []], 200);
      } else {
        return response(['error' => false, 'message' => 'Business Group Ditemukan', 'data' => $item], 200);
      }
    }

    public function getCities()
    {
      $item = City::orderBy('city_id', 'DESC')->get();
      if (count($item) <= 0) {
        # code...
        return response(['error' => true, 'message' => 'Wilayah Kosong', 'data' => []], 200);
      } else {
        return response(['error' => false, 'message' => 'Wilayah Ditemukan', 'data' => $item], 200);
      }
    }

    public function getCategoryPrice()
    {
      $item = CategoryPrice::orderBy('category_price_id', 'DESC')->get();
      if (count($item) <= 0) {
        # code...
        return response(['error' => true, 'message' => 'Category Price Kosong', 'data' => []], 200);
      } else {
        return response(['error' => false, 'message' => 'Category Price Ditemukan', 'data' => $item], 200);
      }
    }

    public function addCustomer(Request $request)
    {
      $valid = Validator::make($request->all(), [
        'customer_name' => 'required',
        'busines_id' => 'required',
        'customer_group_id' => 'required',
        'customer_address' => 'required|string',
        'customer_store' => 'required',
        'customer_telp' => 'required',
        'customer_hp' => 'required',
        'customer_npwp' => 'required',
        'customer_npwp_name' => 'required',
        'customer_npwp_address' => 'required',
        'customer_mail' => 'required',
        'city_id' => 'required',
        'customer_img' => 'required|image|mimes:jpeg,jpg,png',
        'category_price_id' => 'required',
        'customer_limit_kredit' => 'required',
        'customer_limit_card' => 'required',
        'customer_limit_day' => 'required',
        'customer_card_no' => 'required',
        'customer_pic_name' => 'required',
        'customer_pic_telp' => 'required',
        'customer_pic_address' => 'required'
      ]);

      if ($valid->passes()) {
        # code...
        $cekIfNPWPExists = Customer::where('customer_npwp', $request->customer_npwp)->first();
        if (count($cekIfNPWPExists) >= 1) {
          # code...
          return response(['error' => true, 'message' => 'NPWP Sudah Terdaftar'], 200);
        } else {
          $busines_id = Business::select('busines_id')->where('busines_name', $request->busines_id)->first();
          $customer_group_id = CustomerGroup::select('customer_group_id')->where('customer_group_name', $request->customer_group_id)->first();
          $city_id = City::select('city_id')->where('city_name', $request->city_id)->first();
          $category_price_id = CategoryPrice::select('category_price_id')->where('category_price_name', $request->category_price_id)->first();

          $create = Customer::create([
            'customer_name' => $request->customer_name,
            'busines_id' => $busines_id->busines_id,
            'customer_group_id' => $customer_group_id->customer_group_id,
            'customer_address' => $request->customer_address,
            'customer_telp' => $request->customer_telp,
            'customer_store' => $request->customer_store,
            'customer_hp' => $request->customer_hp,
            'customer_npwp' => $request->customer_npwp,
            'customer_npwp_name' => $request->customer_npwp_name,
            'customer_npwp_address' => $request->customer_npwp_address,
            'customer_mail' => $request->customer_mail,
            'city_id' => $city_id->city_id,
            'customer_img' => ($request->hasFile('customer_img') ? $this->savePhoto($request->file('customer_img')) : NULL),
            'category_price_id' => $category_price_id->category_price_id,
            'customer_limit_kredit' => $request->customer_limit_kredit,
            'customer_limit_card' => 0,
            'customer_limit_day' => $request->customer_limit_day,
            'customer_card_no' => 0,
            'customer_pic_name' => $request->customer_name,
            'customer_pic_telp' => $request->customer_telp,
            'customer_pic_address' => $request->customer_pic_address
          ]);

          if ($create) {
            # code...
            return response(['error' => false, 'message' => 'Customer Berhasil Dibuat'], 200);
          } else {
            return response(['error' => true, 'message' => 'Gagal Membuat Customer'], 200);
          }
        }
      } else {
        return response(['error' => true, 'message' => 'Data Tidak Valid'], 200);
      }
    }

    public function scanBarcode($id)
    {
      $cek = Barang::where('item_barcode', base64_decode($id))->first();
      if (count($cek) <= 0) {
        # code...
        return response(['error' => true, 'message' => 'Barcode Tidak Ditemukan'], 200);
      } else {
        return response(['error' => false, 'message' => 'Barcode Ditemukan'], 200);
      }
    }

    public function getListWarehouse()
    {
      $item = Warehouse::select('warehouse_id', 'warehouse_name')->orderBy('warehouse_id', 'DESC')->get();
      if (count($item) <= 0) {
        # code...
        return response(['error' => true, 'message' => 'Warehouse Tidak Ditemukan', 'data' => []], 200);
      } else {
        return response(['error' => false, 'message' => 'Warehouse Ditemukan', 'data' => $item], 200);
      }
    }

    public function getPelunasanByCabang(Request $request)
    {
      $valid = Validator::make($request->all(), [
        'warehouse_name' => 'required'
      ]);

      if ($valid->passes()) {
        # code...
        $wh = Warehouse::select('warehouse_id')->where('warehouse_name', $request->warehouse_name)->first();
        if ($wh == null || $wh == "" || empty($wh)) {
          # code...
          return response(['error' => true, 'message' => 'Warehouse Tidak Ada', 'data' => [], 'total' => 0], 200);
        } else {
          $notas = Nota::where('warehouse_id', $wh->warehouse_id)->where('nota_status', 0)->orderBy('nota_id', 'DESC')->get();
          $totalNetto = Nota::where('warehouse_id', $wh->warehouse_id)->where('nota_status', 0)->sum('nota_netto');
          $totalDp = Nota::where('warehouse_id', $wh->warehouse_id)->where('nota_status', 0)->sum('nota_dp');
          $totalBayar = $totalNetto - $totalDp;
          if (count($notas) <= 0) {
            # code...
            return response(['error' => true, 'message' => 'Nota Tidak Ditemukan', 'data' => [], 'total' => 0], 200);
          } else {
            return response(['error' => false, 'message' => 'Nota Ditemukan', 'data' => $notas, 'total' => $totalBayar], 200);
          }
        }
      } else {
        return response(['error' => true, 'message' => 'Warehouse Belum Dipilih', 'data' => [], 'total' => 0], 200);
      }
    }

    public function getListCustomer()
    {
      $item = Customer::select('customer_id', 'customer_name')->orderBy('customer_id', 'DESC')->get();
      if (count($item) <= 0) {
        # code...
        return response(['error' => true, 'message' => 'Customer Tidak Ditemukan', 'data' => []], 200);
      } else {
        return response(['error' => false, 'message' => 'Customer Ditemukan', 'data' => $item], 200);
      }
    }

    public function getPiutangByCustomer(Request $request)
    {
      $valid = Validator::make($request->all(), [
        'customer_name' => 'required'
      ]);

      if ($valid->passes()) {
        # code...
        $cs = Customer::select('customer_id')->where('customer_name', $request->customer_name)->first();
        if ($cs == null || $cs == "" || empty($cs)) {
          # code...
          return response(['error' => true, 'message' => 'Customer Tidak Ada', 'data' => [], 'total' => 0], 200);
        } else {
          $notas = Nota::where('customer_id', $cs->customer_id)->where('nota_status', 0)->orderBy('nota_id', 'DESC')->get();
          $totalNetto = Nota::where('customer_id', $cs->customer_id)->where('nota_status', 0)->sum('nota_netto');
          $totalDp = Nota::where('customer_id', $cs->customer_id)->where('nota_status', 0)->sum('nota_dp');
          $totalBayar = $totalNetto - $totalDp;
          if (count($notas) <= 0) {
            # code...
            return response(['error' => true, 'message' => 'Nota Tidak Ditemukan', 'data' => [], 'total' => 0], 200);
          } else {
            return response(['error' => false, 'message' => 'Nota Ditemukan', 'data' => $notas, 'total' => $totalBayar], 200);
          }
        }
      } else {
        return response(['error' => true, 'message' => 'Customer Belum Dipilih', 'data' => [], 'total' => 0], 200);
      }
    }

    public function getFilterPiutang($kode_nota, $date, $customer_name)
    {
      if (!empty($kode_nota) && !empty($date) && !empty($customer_name)) {
        # code...
        $cs = Customer::select('customer_id')->where('customer_name', $customer_name)->first();
        if ($cs == null || $cs == "" || empty($cs)) {
          # code...
          return response(['error' => true, 'message' => 'Customer Tidak Ada', 'data' => [], 'total' => 0], 200);
        } else {
          $notas = Nota::where('customer_id', $cs->customer_id)->where('nota_status', 0)->where('nota_code', 'LIKE', '%'.$kode_nota.'%')->whereDate('nota_date', date('Y-m-d', strtotime($date)))->orderBy('nota_id', 'DESC')->get();
          $totalNetto = Nota::where('customer_id', $cs->customer_id)->where('nota_status', 0)->sum('nota_netto');
          $totalDp = Nota::where('customer_id', $cs->customer_id)->where('nota_status', 0)->sum('nota_dp');
          $totalBayar = $totalNetto - $totalDp;
          if (count($notas) <= 0) {
            # code...
            return response(['error' => true, 'message' => 'Nota Tidak Ditemukan', 'data' => [], 'total' => 0], 200);
          } else {
            return response(['error' => false, 'message' => 'Nota Ditemukan', 'data' => $notas, 'total' => $totalBayar], 200);
          }
        }
      } else {
        return response(['error' => true, 'message' => 'Kode Nota Masih Kosong', 'data' => [], 'total' => 0], 200);
      }
    }

    public function getFilterPelunasan($kode_nota, $date, $warehouse_name)
    {
      if (!empty($kode_nota) && !empty($date) && !empty($warehouse_name)) {
        # code...
        $wh = Warehouse::select('warehouse_id')->where('warehouse_name', $warehouse_name)->first();
        if ($wh == null || $wh == "" || empty($wh)) {
          # code...
          return response(['error' => true, 'message' => 'Warehouse Tidak Ada', 'data' => [], 'total' => 0], 200);
        } else {
          $notas = Nota::where('warehouse_id', $wh->warehouse_id)->where('nota_status', 0)->where('nota_code', 'LIKE', '%'.$kode_nota.'%')->whereDate('nota_date', date('Y-m-d', strtotime($date)))->orderBy('nota_id', 'DESC')->get();
          $totalNetto = Nota::where('warehouse_id', $wh->warehouse_id)->where('nota_status', 0)->sum('nota_netto');
          $totalDp = Nota::where('warehouse_id', $wh->warehouse_id)->where('nota_status', 0)->sum('nota_dp');
          $totalBayar = $totalNetto - $totalDp;
          if (count($notas) <= 0) {
            # code...
            return response(['error' => true, 'message' => 'Nota Tidak Ditemukan', 'data' => [], 'total' => 0], 200);
          } else {
            return response(['error' => false, 'message' => 'Nota Ditemukan', 'data' => $notas, 'total' => $totalBayar], 200);
          }
        }
      } else {
        return response(['error' => true, 'message' => 'Warehouse Belum Dipilih', 'data' => [], 'total' => 0], 200);
      }
    }

    public function getBarangImage($id) {
      if (empty($id)) {
        # code...
        return response(['error' => true, 'message' => 'Parameter Masih Kosong', 'data' => []], 200);
      } else {
        $item = ItemGalery::select('item_galery_file')->where('item_id', $id)->get();

        if (count($item) <= 0) {
          # code...
          return response(['error' => true, 'message' => 'Barang Tidak Memiliki Foto', 'data' => []], 200);
        } else {
          return response(['error' => false, 'message' => 'Foto Ditemukan', 'data' => $item], 200);
        }
      }
    }

    public function getKonfirmasiDO($delivery_id) {
      if (empty($delivery_id)) {
        # code...
        return response(['error' => true, 'message' => 'Parameter Masih Kosong', 'data' => [], 'item' => []], 200);
      } else {
        $deliveryItem = \DB::table('deliveries')->select('deliveries.*', 'delivery_details.delivery_detail_id','delivery_details.delivery_detail_code','delivery_details.delivery_detail_type','warehouses.warehouse_name','delivery_details.delivery_detail_status')->join('delivery_details', 'delivery_details.delivery_id', '=', 'deliveries.delivery_id')->join('warehouses', 'warehouses.warehouse_id', '=', 'delivery_details.warehouse_id')->where('deliveries.delivery_id', 1)->first();

        $delivery = \DB::table('deliveries')->select('deliveries.*','employees.employee_name','customers.customer_name','customers.customer_address', 'notas.nota_status')->join('employees', 'employees.employee_id', '=', 'deliveries.employee_id')->join('notas', 'notas.nota_id', 'deliveries.nota_id')->join('customers', 'customers.customer_id', 'notas.customer_id')->where('deliveries.delivery_id', $delivery_id)->first();

        $item = \DB::table('delivery_sends')->select('delivery_sends.*', 'items.item_name', 'items.item_per_unit', 'units.unit_name', 'foreman_details.foreman_detail_qty', 'nota_detail_orders.nota_detail_order_qty', 'nota_detail_orders.nota_detail_order_now')->join('nota_detail_orders', 'nota_detail_orders.nota_detail_order_id', 'delivery_sends.nota_detail_order_id')->join('items', 'items.item_id', 'nota_detail_orders.item_id')->join('units', 'units.unit_id', 'items.unit_id')->join('foreman_details', 'foreman_details.delivery_send_id', 'delivery_sends.delivery_send_id')->where('delivery_sends.delivery_detail_id', $deliveryItem->delivery_detail_id)->get();

        if (count($delivery) <= 0) {
          # code...
          return response(['error' => true, 'message' => 'Data Kosong', 'namaCustomer' => 'Tidak Ditemukan', 'alamatCustomer' => 'Tidak Ditemukan', 'hargaOngkir' => 0, 'tanggalDo' => '00-00-0000', 'namaSupir' => 'Tidak Ditemukan', 'status' => 0, 'item' => []], 200);
        } else {
          return response(['error' => false, 'message' => 'Data Ditemukan', 'namaCustomer' => $delivery->customer_name, 'alamatCustomer' => $delivery->customer_address, 'hargaOngkir' => $delivery->delivery_cost, 'tanggalDo' => $delivery->delivery_date, 'namaSupir' => $delivery->employee_name, 'status' => $delivery->nota_status , 'item' => $item], 200);
        }
      }
    }

    public function updateDeliveredStock(Request $request) {
      $valid = Validator::make($request->all(), [
        'order_id' => 'required',
        'quantity' => 'required'
      ]);

      if ($valid->passes()) {
        # code...
        $cek = \DB::table('nota_detail_orders')->where('nota_detail_order_id', $request->order_id)->first();
        // dd($cek);
        if (empty($cek) || $cek == null) {
          # code...
          return response(['error' => true, 'message' => 'Barang Tidak Ada'], 200);
        } else {
          if ($request->quantity > $cek->nota_detail_order_qty) {
            # code...
            return response(['error' => true, 'message' => 'Quantity Tidak Boleh Lebih Dari Orderan'], 200);
          }

          $update = \DB::table('nota_detail_orders')->where('nota_detail_order_id', $request->order_id)->update([
            'nota_detail_order_now' => $request->quantity
          ]);

          if ($update) {
            # code...
            return response(['error' => false, 'message' => 'Quantity Berhasil Diupdate'], 200);
          } else {
            return response(['error' => true, 'message' => 'Gagal Update Qty'], 200);
          }
        }
      } else {
        return response(['error' => true, 'message' => 'Gagal Update Quantity'], 200);
      }
    }

    public function setConfirmedDo(Request $request) {
      $valid = Validator::make($request->all(), [
        'delivery_id' => 'required'
      ]);

      if ($valid->passes()) {
        # code...
        $cek = Delivery::where('delivery_id', $request->delivery_id)->first();
        if (empty($cek) || $cek == null) {
          # code...
          return response(['error' => true, 'message' => 'DO Tidak Ditemukan'], 200);
        } else {
          $update = Nota::where('nota_id', $cek->nota_id)->update(['nota_status' => 1]);
          if ($update) {
            # code...
            return response(['error' => false, 'message' => 'DO Berhasil Dikonfirmasi'], 200);
          } else {
            return response(['error' => true, 'message' => 'DO Sudah Terkonfirmasi'], 200);
          }
        }
      } else {
        return response(['error' => true, 'message' => 'Parameter Masih Kosong'], 200);
      }
    }

    protected function savePhoto($photo)
    {
        $destinationPath = 'images';
        $subdestinationPath = 'customer';
        $extension = $photo->getClientOriginalExtension();
        $fileName = rand(11111,99999).'.'.$extension;
        // $fileName = array_pop(explode(DIRECTORY_SEPARATOR, $photo));
        $photo->move($destinationPath. '/' . $subdestinationPath , $fileName);
        $create['customer_img'] = $destinationPath. '/' . $subdestinationPath . '/' . $fileName;

        return $create['customer_img'];
    }

    protected function deletePhoto($photo)
    {
        File::delete($photo);
        return $photo;
    }
}